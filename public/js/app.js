window.onload = function(){
    new Vue({
        el: '#palette-extractor-app',
        data: {
            imageSrc: 'test2.jpg',
            colors: ["#1c4624","#015164","#080809","#bc344a","#551819"],
            hasImage: false,
            loading: false
        },
        methods: {
            imageUploaded: function(e) {
                this.imageSrc = URL.createObjectURL(e.target.files[0])
                this.hasImage = true
                this.colors = []
            },

            exportImage: function(e){
                e.preventDefault();
                html2canvas(document.getElementById("img-container")).then(function(canvas) {
                    img = canvas.toDataURL()
                    imgEl = '<img src="' + img + '"></img>'
                    newWindow = window.open();
                    newWindow.document.open();
                    newWindow.document.write(imgEl);
                    newWindow.document.close();
                });
            },

            formSubmitted: function(e) {
                e.preventDefault();

                this.loading = true;

                file = document.getElementById('file').files[0];
                color_number = document.getElementById('color_number').value
                formData = new FormData();
                formData.append('file', file)
                formData.append('color_number', color_number)

                xhr = new XMLHttpRequest();
                xhr.open('POST', '/upload')
                xhr.send(formData)

                xhr.onload = function() {
                    if (xhr.status === 200) {
                        var response = JSON.parse(xhr.responseText);
                        this.colors  = response.colors;
                        this.loading = false;
                    }
                }.bind(this)
            }
        }
    })
}
    
Vue.component('color-panel', {
    props: ['color'],
    template: '<div class="flex-grow" :style="{ backgroundColor: color }"><br><br></div>'
})