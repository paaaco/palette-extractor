require File.expand_path '../spec_helper.rb', __FILE__

describe "Palette Extractor App" do
  describe "GET /" do
    it "should allow accessing the home page" do
      get '/'
      expect(last_response).to be_ok
    end
  end

  describe "POST /upload" do
    subject { JSON.parse last_response.body }

    context "when no parameters is passed" do
      it "should not show an error" do
        post '/upload'
        expect(last_response).to be_ok
      end

      it "should only return an empty array of colors" do
        post '/upload'
        expect(subject["colors"]).to eq([])
      end
    end

    context "when parameters is passed" do
      context "when file is uploaded" do
        let(:expected_output) { ["#5c2d20", "#241510", "#6a4936", "#5f5650", "#3e403f"] }
        let(:file_path) { ["#{Sinatra::Application.settings.root}/spec/test0.png", "image/png"] }

        before :each do 
          post "/upload", file: Rack::Test::UploadedFile.new(*file_path)
        end

        it "should be a valid request" do
          expect(last_response).to be_ok
        end

        it "should give 5 colors by default" do
          expect(subject["colors"].count).to eq 5
        end

        it "should return the prominent colors" do
          expect(subject["colors"]).to eq expected_output
        end

        context "when number of colors is specified" do
          before :each do 
            post "/upload", file: Rack::Test::UploadedFile.new(*file_path), color_number: 7
          end

          it "should return that much colors" do
            expect(subject["colors"].count).to eq 7
          end
        end 
      end
    end
  end
end

