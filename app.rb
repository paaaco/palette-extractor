require 'sinatra'
require 'sass'
require 'camalian'
require 'pry'
require 'slim'

get '/' do
  slim :index
end

post '/upload' do
  content_type :json

  file = params.dig(:file, :tempfile)
  if file 
    color_number = params[:color_number] || 5
    img = Camalian::load(file.path)
    colors = img.prominent_colors(color_number.to_i).sort_similar_colors.map(&:to_hex)
    
    { colors: colors }.to_json
  else
    { colors: [] }.to_json
  end
end

get '/styles.css' do
  content_type "text/css"
  scss :styles, views: "#{settings.root}/assets/stylesheets", style: :expanded
end