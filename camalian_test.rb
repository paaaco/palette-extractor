require 'camalian'

image = Camalian::load('test.png')
colors = image.prominent_colors(7)
colors = colors.sort_similar_colors

puts colors.map(&:to_hex)
